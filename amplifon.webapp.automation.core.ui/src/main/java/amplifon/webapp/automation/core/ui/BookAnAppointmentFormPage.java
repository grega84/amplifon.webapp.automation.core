package amplifon.webapp.automation.core.ui;


import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BookAnAppointmentFormPage extends LoadableComponent<BookAnAppointmentFormPage>{
private WebDriver driver;
private int driverType;

private WebElement errorsList;
private WebElement titleFieldMr;
private WebElement titleFieldMiss;
private WebElement firstNameInput;
private WebElement surnameInput;
private WebElement emailInput;
private WebElement phoneInput;
private WebElement privacyCheckBox;
private WebElement baButton;

public BookAnAppointmentFormPage(WebDriver driver,int driverType) {
		this.driver=driver;
this.driverType=driverType;
	
	}

@Override
	protected void isLoaded() throws Error {
		  PageFactory.initElements(driver, this);

	}

@Override
	protected void load() 
	{
switch(this.driverType)
		{
			case 0://android driver
			
			break;
			case 1://ios driver
			
			break;
			case 2://web driver
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.BookAnAppointmentFormPageLocator.TITLEFIELDMR.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.BookAnAppointmentFormPageLocator.TITLEFIELDMISS.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.BookAnAppointmentFormPageLocator.FIRSTNAMEINPUT.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.BookAnAppointmentFormPageLocator.SURNAMEINPUT.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.BookAnAppointmentFormPageLocator.EMAILINPUT.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.BookAnAppointmentFormPageLocator.PHONEINPUT.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.BookAnAppointmentFormPageLocator.PRIVACYCHECKBOX.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.BookAnAppointmentFormPageLocator.BABUTTON.getWebLocator())));

			break;
		}	}
}
