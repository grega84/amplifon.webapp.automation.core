package amplifon.webapp.automation.core.ui;


import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RequestAnAppointmentPage extends LoadableComponent<RequestAnAppointmentPage>{
private WebDriver driver;
private int driverType;

private WebElement selectAStoreInput;
private WebElement searchButton;
private WebElement storeNameSearch;
private WebElement timeSlots;
private WebElement timeSlotSelected;
private WebElement calendarDays;

public RequestAnAppointmentPage(WebDriver driver,int driverType) {
		this.driver=driver;
this.driverType=driverType;
	
	}

@Override
	protected void isLoaded() throws Error {
		  PageFactory.initElements(driver, this);

	}

@Override
	protected void load() 
	{
switch(this.driverType)
		{
			case 0://android driver
			
			break;
			case 1://ios driver
			
			break;
			case 2://web driver
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.RequestAnAppointmentPageLocator.SELECTASTOREINPUT.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.RequestAnAppointmentPageLocator.SEARCHBUTTON.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.RequestAnAppointmentPageLocator.TIMESLOTS.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.RequestAnAppointmentPageLocator.TIMESLOTSELECTED.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.RequestAnAppointmentPageLocator.CALENDARDAYS.getWebLocator())));

			break;
		}	}
}
