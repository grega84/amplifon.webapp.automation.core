package amplifon.webapp.automation.core.ui;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.text.StrSubstitutor;

import scala.annotation.meta.param;

public class Locators {
public interface ILocator
	{
		public String getAndroidLocator();
		public String getIOSLocator();
		public String getWebLocator();
	}

public enum HomePageLocator implements ILocator
{

REQUESTANAPPOINTMENTLINK("","","//a[contains(.,'Request appointment')]"),
COOKIEBUTTON("","","//button[@class='cookie-notification-btn cta-btn cta-primay-white-tr-two']");

	    private String locatorAndroid;
	    private String locatorIOS;
	    private String locatorWeb;

	    HomePageLocator(String locatorAndroid, String locatorIOS, String locatorWeb) {
	        this.locatorAndroid = locatorAndroid;
	        this.locatorIOS=locatorIOS;
	        this.locatorWeb=locatorWeb;
	    }
	    
	    public String getAndroidLocator()
	    {
			return this.locatorAndroid;
	    }
	    
	    public String getIOSLocator()
	    {
			return this.locatorIOS;
	    }
	    public String getWebLocator()
	    {
			return this.locatorWeb;
	    }
}

public enum ThankYouPageLocator implements ILocator
{

HEADER("","","//span[@class='main-title__name']"),
MESSAGE("","","//h3[@class='main-title green-text thank-appointment']"),
YOURAPPOINTMENTTITLE("","","//p[@class='title--h5 conf-app-info']"),
CONFIRMATIONEMAILMESSAGE("","","//p[contains(text(),\"We've send you a confirmation email.\")]"),
CONFIRMATIONEMAILHELPMESSAGE("","","//p[contains(text(),'You can help us to provide the best service possib')]"),
DATEANDTIMEAPPOINTMENT("","","//p[@class='title--h3 conf-app-detail']"),
AUDIOLOGIST("","","//p[@class='title--h5 conf-app-doc']"),
STORENAME("","","//h3[@class='title--h4 store-name']"),
STOREADDRESS("","","//p[@class='title--h5 store-address']"),
STOREPHONENUMBER("","","//span[@class='store-phone-number']"),
PRINTCONFIRMATIONBUTTON("","","//a[@class='service-cta print-cta']"),
SENDEMAILBUTTON("","","//a[@class='service-cta send-email-cta']");

	    private String locatorAndroid;
	    private String locatorIOS;
	    private String locatorWeb;

	    ThankYouPageLocator(String locatorAndroid, String locatorIOS, String locatorWeb) {
	        this.locatorAndroid = locatorAndroid;
	        this.locatorIOS=locatorIOS;
	        this.locatorWeb=locatorWeb;
	    }
	    
	    public String getAndroidLocator()
	    {
			return this.locatorAndroid;
	    }
	    
	    public String getIOSLocator()
	    {
			return this.locatorIOS;
	    }
	    public String getWebLocator()
	    {
			return this.locatorWeb;
	    }
}

public enum RequestAnAppointmentPageLocator implements ILocator
{

SELECTASTOREINPUT("","","//input[@id='location-input']"),
SEARCHBUTTON("","","//button[@class='sl-search-button btn red-btn--fill']"),
STORENAMESEARCH("","","//p[contains(@class,'store-name')]/strong[contains(.,'$(storeName)')]"),
TIMESLOTS("","","//a[contains(@class,'baa-hour-app')]"),
TIMESLOTSELECTED("","","//a[@class='baa-hour-app selected']"),
CALENDARDAYS("","","//div[@class='baa-calendar-day']");

	    private String locatorAndroid;
	    private String locatorIOS;
	    private String locatorWeb;

	    RequestAnAppointmentPageLocator(String locatorAndroid, String locatorIOS, String locatorWeb) {
	        this.locatorAndroid = locatorAndroid;
	        this.locatorIOS=locatorIOS;
	        this.locatorWeb=locatorWeb;
	    }
	    
	    public String getAndroidLocator()
	    {
			return this.locatorAndroid;
	    }
	    
	    public String getIOSLocator()
	    {
			return this.locatorIOS;
	    }
	    public String getWebLocator()
	    {
			return this.locatorWeb;
	    }
}

public enum CyclePageLocator implements ILocator
{

ACTUALDATE("","","//td[@class='blue_bold_txt']"),
SHOWSTORE("","","//td[contains(text(),'show store:')]"),
SHOWSTOREFIELD("","","//select[@id='clinic_id']"),
SHOWCONSULTANT("","","//td[contains(text(),'show consultant:')]"),
SHOWCONSULTANTFIELD("","","//select[@name='provider_id']"),
FINDAPPOINTMENTTYPE("","","//td[contains(text(),'find appointment type:')]"),
FINDAPPOINTMENTTYPEFIELD("","","//select[@name='appt_type_id']"),
LENGTHFIELD("","","//input[@name='appt_length']"),
FINDTEXT("","","//input[@name='find_next']"),
LEFTROWS("","","//a[contains(text(),'<<')]"),
RIGHTROWS("","","//a[contains(text(),'>>')]"),
MONTHCALENDAR("","","//select[@id='currMonth']"),
YEARCALENDAR("","","//select[@id='currYear']"),
CURRENTSELECTEDDATE("","","//font[@class='calselect']"),
CALENDARDATE("","","//font[@class='calnotselect']"),
APPOINTMENTHOURS("","","//tr/td[text()='9:00 am']"),
APPOINTMENTSLOTS("","","//tr/td[text()='9:00 am']/following-sibling::td");

	    private String locatorAndroid;
	    private String locatorIOS;
	    private String locatorWeb;

	    CyclePageLocator(String locatorAndroid, String locatorIOS, String locatorWeb) {
	        this.locatorAndroid = locatorAndroid;
	        this.locatorIOS=locatorIOS;
	        this.locatorWeb=locatorWeb;
	    }
	    
	    public String getAndroidLocator()
	    {
			return this.locatorAndroid;
	    }
	    
	    public String getIOSLocator()
	    {
			return this.locatorIOS;
	    }
	    public String getWebLocator()
	    {
			return this.locatorWeb;
	    }
}

public enum BookAnAppointmentFormPageLocator implements ILocator
{

ERRORSLIST("","","//ul[contains(@id,'parsley-id')]"),
TITLEFIELDMR("","","(//input[@name='customer-title'])[1]"),
TITLEFIELDMISS("","","(//input[@name='customer-title'])[2]"),
FIRSTNAMEINPUT("","","//input[@id='first-name']"),
SURNAMEINPUT("","","//input[@id='last-name']"),
EMAILINPUT("","","//input[@id='email-field']"),
PHONEINPUT("","","//input[@id='phone-number']"),
PRIVACYCHECKBOX("","","//input[./following-sibling::*[@for='privacy']]"),
BABUTTON("","","//div[contains(@class,'book-an-appointment-btn')]");

	    private String locatorAndroid;
	    private String locatorIOS;
	    private String locatorWeb;

	    BookAnAppointmentFormPageLocator(String locatorAndroid, String locatorIOS, String locatorWeb) {
	        this.locatorAndroid = locatorAndroid;
	        this.locatorIOS=locatorIOS;
	        this.locatorWeb=locatorWeb;
	    }
	    
	    public String getAndroidLocator()
	    {
			return this.locatorAndroid;
	    }
	    
	    public String getIOSLocator()
	    {
			return this.locatorIOS;
	    }
	    public String getWebLocator()
	    {
			return this.locatorWeb;
	    }
}

}
