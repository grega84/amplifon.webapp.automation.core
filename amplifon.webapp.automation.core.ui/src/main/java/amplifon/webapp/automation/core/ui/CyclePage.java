package amplifon.webapp.automation.core.ui;


import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CyclePage extends LoadableComponent<CyclePage>{
private WebDriver driver;
private int driverType;

private WebElement actualDate;
private WebElement showStore;
private WebElement showStoreField;
private WebElement showConsultant;
private WebElement showConsultantField;
private WebElement findAppointmentType;
private WebElement findAppointmentTypeField;
private WebElement lengthField;
private WebElement findText;
private WebElement leftRows;
private WebElement rightRows;
private WebElement monthCalendar;
private WebElement yearCalendar;
private WebElement currentSelectedDate;
private WebElement calendarDate;
private WebElement appointmentHours;
private WebElement appointmentSlots;

public CyclePage(WebDriver driver,int driverType) {
		this.driver=driver;
this.driverType=driverType;
	
	}

@Override
	protected void isLoaded() throws Error {
		  PageFactory.initElements(driver, this);

	}

@Override
	protected void load() 
	{
switch(this.driverType)
		{
			case 0://android driver
			
			break;
			case 1://ios driver
			
			break;
			case 2://web driver
			
			break;
		}	}
}
