package amplifon.webapp.automation.core.ui;


import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ThankYouPage extends LoadableComponent<ThankYouPage>{
private WebDriver driver;
private int driverType;

private WebElement header;
private WebElement message;
private WebElement yourAppointmentTitle;
private WebElement confirmationEmailMessage;
private WebElement confirmationEmailHelpMessage;
private WebElement dateAndTimeAppointment;
private WebElement audiologist;
private WebElement storeName;
private WebElement storeAddress;
private WebElement storePhoneNumber;
private WebElement printConfirmationButton;
private WebElement sendEmailButton;

public ThankYouPage(WebDriver driver,int driverType) {
		this.driver=driver;
this.driverType=driverType;
	
	}

@Override
	protected void isLoaded() throws Error {
		  PageFactory.initElements(driver, this);

	}

@Override
	protected void load() 
	{
switch(this.driverType)
		{
			case 0://android driver
			
			break;
			case 1://ios driver
			
			break;
			case 2://web driver
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.ThankYouPageLocator.HEADER.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.ThankYouPageLocator.MESSAGE.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.ThankYouPageLocator.YOURAPPOINTMENTTITLE.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.ThankYouPageLocator.CONFIRMATIONEMAILMESSAGE.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.ThankYouPageLocator.CONFIRMATIONEMAILHELPMESSAGE.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.ThankYouPageLocator.DATEANDTIMEAPPOINTMENT.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.ThankYouPageLocator.AUDIOLOGIST.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.ThankYouPageLocator.STORENAME.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.ThankYouPageLocator.STOREADDRESS.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.ThankYouPageLocator.STOREPHONENUMBER.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.ThankYouPageLocator.PRINTCONFIRMATIONBUTTON.getWebLocator())));
UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(Locators.ThankYouPageLocator.SENDEMAILBUTTON.getWebLocator())));

			break;
		}	}
}
